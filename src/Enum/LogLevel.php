<?php

namespace SDN3Q\Enum;

class LogLevel {

	const DEBUG   = 'debug';
	const INFO    = 'info';
	const WARNING = 'warning';
	const ERROR   = 'error';
}
