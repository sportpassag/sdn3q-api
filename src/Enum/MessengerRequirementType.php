<?php

namespace SDN3Q\Enum;

class MessengerRequirementType {

	const TYPE_MAP = [
		'FileNew'            => [1, 'file.new'],
		'ChannelFaultSignal' => [2, 'channel.faultsignal'],
		'FilePipelineNew'    => [3, 'file.pipeline.new'],
		'FileMetaChanged'    => [4, 'file.meta.changed'],
	];

	const FileNew            = 'FileNew';
	const ChannelFaultSignal = 'ChannelFaultSignal';
	const FilePipelineNew    = 'FilePipelineNew';
	const FileMetaChanged    = 'FileMetaChanged';

	public static function getId($type) {
		return self::TYPE_MAP[$type][0];
	}

	public static function getEventName($type) {
		return self::TYPE_MAP[$type][1];
	}
}
