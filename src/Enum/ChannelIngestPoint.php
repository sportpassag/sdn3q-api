<?php

namespace SDN3Q\Enum;

class ChannelIngestPoint {

	const US_ORIGIN_LIVE   = 15;
	const AT_ORIGIN_LIVE   = 21;
	const DE_ORIGIN_LIVE   = 23;
	const DE_ORIGIN_LIVE_2 = 25;
}
