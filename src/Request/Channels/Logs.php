<?php

namespace SDN3Q\Request\Channels;

use MintWare\DMM\ObjectMapper;
use MintWare\DMM\Serializer\JsonSerializer;
use SDN3Q\Request\BaseRequest;
use SDN3Q\Model\ChannelLog;

class Logs extends BaseRequest {

	protected static $endpoint = 'channels';

	/**
	 * @param int   $channelId
	 * @param array $params
	 *
	 * @return array
	 * @throws \Exception
	 */
	public static function getLogs(int $channelId, array $params = []): array {
		self::$requestParmAsJson  = false;
		self::$requestParmAsQuery = true;

		$logs               = [];
		parent::$subUrl     = $channelId . '/log';
		self::$possibleParm = ['Limit', 'Offset', 'LogLevel'];

		foreach ($params as $key => $value) {
			self::$requestParm[$key] = $value;
		}

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();
			if ($data = json_decode($response, true)) {
				foreach ($data as $log) {
					$logs[] = $mapper->mapDataToObject($log, ChannelLog::class);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}

		return $logs;
	}

	/**
	 * @param int   $channelId
	 * @param array $params
	 *
	 * @return array
	 * @throws \Exception
	 */
	public static function getLogsStartingFromLevel(int $channelId, string $level, array $params = []): array {
		self::$requestParmAsJson  = false;
		self::$requestParmAsQuery = true;

		$logs               = [];
		parent::$subUrl     = $channelId . '/log/' . $level;
		self::$possibleParm = ['Limit', 'Offset'];

		foreach ($params as $key => $value) {
			self::$requestParm[$key] = $value;
		}

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();
			if ($data = json_decode($response, true)) {
				foreach ($data as $log) {
					$logs[] = $mapper->mapDataToObject($log, ChannelLog::class);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}

		return $logs;
	}
}
