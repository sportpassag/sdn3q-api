<?php

namespace SDN3Q\Request\Channels;

use MintWare\DMM\ObjectMapper;
use MintWare\DMM\Serializer\JsonSerializer;
use SDN3Q\Model\ChannelIngest;
use SDN3Q\Model\ChannelStatus;
use SDN3Q\Model\IngestPoint;
use SDN3Q\Request\BaseRequest;

class Ingest extends BaseRequest {

	protected static $endpoint = 'channels';

	/**
	 * Return Ingest of a Channel
	 *
	 * @param int $channelId
	 *
	 * @return ChannelIngest
	 * @throws \Exception
	 */
	public static function getIngest(int $channelId) {
		parent::$subUrl = $channelId . '/ingest';

		try {
			$mapper   = new ObjectMapper(new JsonSerializer());
			$response = self::getResponse();
			$input    = $mapper->map($response, ChannelIngest::class);
		} catch (\Exception $e) {
			throw $e;
		}

		return $input;
	}

	/**
	 * Change Channel Ingest
	 *
	 * @param int    $channelId
	 * @param string $streamInType
	 * @param bool   $useIngestVersion2
	 * @param int    $timeshiftDuration
	 * @param int    $srtRecoveryBuffer
	 * @param bool   $srtPasswordProtection
	 * @param bool   $automaticTranscoding
	 * @param bool   $ingestStabilizing
	 * @param bool   $ingestFailover
	 *
	 * @return ChannelIngest
	 * @throws \Exception
	 */
	public static function changeIngest(
		int    $channelId,
		string $streamInType,
		bool   $useIngestVersion2 = true,
		int    $timeshiftDuration = 0,
		int    $srtRecoveryBuffer = 0,
		bool   $srtPasswordProtection = false,
		bool   $automaticTranscoding = false,
		bool   $ingestStabilizing = true,
		bool   $ingestFailover = false
	) {
		parent::$subUrl     = $channelId . '/ingest';
		self::$method       = 'put';
		self::$possibleParm = [
			'StreamInType',
			'UseIngestVersion2',
			'TimeshiftDuration',
			'srtRecoveryBuffer',
			'srtPasswordProtection',
			'AutomaticTranscoding',
			'IngestStabilizing',
			'IngestFailover',
		];

		try {
			self::$requestParm['StreamInType']          = $streamInType;
			self::$requestParm['UseIngestVersion2']     = $useIngestVersion2;
			self::$requestParm['TimeshiftDuration']     = $timeshiftDuration;
			self::$requestParm['srtRecoveryBuffer']     = $srtRecoveryBuffer;
			self::$requestParm['srtPasswordProtection'] = $srtPasswordProtection;
			self::$requestParm['AutomaticTranscoding']  = $automaticTranscoding;
			self::$requestParm['IngestStabilizing']     = $ingestStabilizing;
			self::$requestParm['IngestFailover']        = $ingestFailover;

			$mapper   = new ObjectMapper(new JsonSerializer());
			$response = self::getResponse();
			$input    = $mapper->map($response, ChannelIngest::class);
		} catch (\Exception $e) {
			throw $e;
		}

		return $input;
	}

	/**
	 * @param int $channelId
	 * @param int $ingestPointId
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function linkChannelIngestPoint(int $channelId, int $ingestPointId) {
		self::$method   = 'patch';
		parent::$subUrl = $channelId . '/ingest/ingestpoint/' . $ingestPointId;

		try {
			$response = self::getResponse();
		} catch (\Exception $e) {
			throw $e;
		}

		return true;
	}

	/**
	 * @param int $channelId
	 * @param int $ingestPointId
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function unlinkChannelIngestPoint(int $channelId, int $ingestPointId) {
		self::$method   = 'delete';
		parent::$subUrl = $channelId . '/ingest/ingestpoint/' . $ingestPointId;

		try {
			$response = self::getResponse();
		} catch (\Exception $e) {
			throw $e;
		}

		return true;
	}

	/**
	 * @param int $channelId
	 * @param int $ingestPointId
	 *
	 * @return IngestPoint|null
	 * @throws \Exception
	 */
	public static function patchChannelIngestPointPrimary(int $channelId, int $ingestPointId) {
		$ingestPoint    = null;
		self::$method   = 'patch';
		parent::$subUrl = $channelId . '/ingest/ingestpoint/primary/' . $ingestPointId;

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response    = self::getResponse();
			$ingestPoint = $mapper->map($response, IngestPoint::class);
		} catch (\Exception $e) {
			throw $e;
		}

		return $ingestPoint;
	}

	/**
	 * @param int $channelId
	 *
	 * @return ChannelStatus[]|null
	 * @throws \Exception
	 */
	public static function getIngestPointsStatus(int $channelId) {
		$ingestsStatus  = [];
		parent::$subUrl = $channelId . '/ingestpoints/status';

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();
			$data     = json_decode($response, true);
			if (count($data['ChannelIngestPointsStatus'])) {
				foreach ($data['ChannelIngestPointsStatus'] as $ingestPointStatus) {
					$ingestsStatus[] = $mapper->mapDataToObject($ingestPointStatus, ChannelStatus::class);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}

		return $ingestsStatus;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public static function getIngestPoints(): array {
		$ingestPoints   = [];
		parent::$subUrl = 'ingest/ingestpoints';

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();
			$data     = json_decode($response, true);
			if (count($data['IngestPoints'])) {
				foreach ($data['IngestPoints'] as $ingestPoint) {
					$ingestPoints[] = $mapper->mapDataToObject($ingestPoint, IngestPoint::class);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}

		return $ingestPoints;
	}
}
