<?php

namespace SDN3Q\Request\Messengers;

use MintWare\DMM\ObjectMapper;
use MintWare\DMM\Serializer\JsonSerializer;
use SDN3Q\Model\Messenger;
use SDN3Q\Request\BaseRequest;

class Messengers extends BaseRequest {

	protected static $endpoint = 'messengers';

	/**
	 * @return Messenger[]|null
	 * @throws \Exception
	 */
	public static function getMessengers() {
		$messengers = [];

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();
			$data     = json_decode($response, true);
			if (count($data['Messengers']) > 0) {
				foreach ($data['Messengers'] as $messenger) {
					$messengers[] = $mapper->map(json_encode($messenger), Messenger::class);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}

		return $messengers;
	}

	/**
	 * @param int $projectId
	 * @param int $messengerId
	 *
	 * @return Messenger|null
	 * @throws \Exception
	 */
	public static function getMessenger(int $projectId, int $messengerId) {
		$messenger      = null;
		parent::$subUrl = $projectId . '/messenger/' . $messengerId;

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();

			$messenger = $mapper->map($response, Messenger::class);
		} catch (\Exception $e) {
			throw $e;
		}

		return $messenger;
	}

	/**
	 * @param int   $projectId
	 * @param array $params
	 *
	 * @return Messenger|null
	 * @throws \Exception
	 */
	public static function postMessenger(int $projectId, array $params = []) {
		$messenger          = null;
		self::$method       = 'post';
		self::$subUrl       = $projectId . '/messenger';
		self::$requiredParm = ['Name', 'WebhookURI', 'WebhookMethod', 'MessengerRequirement'];

		foreach ($params as $key => $value) {
			self::$requestParm[$key] = $value;
		}

		try {
			$mapper = new ObjectMapper(new JsonSerializer());

			$response = self::getResponse();

			$messenger = $mapper->map($response, Messenger::class);
		} catch (\Exception $e) {
			throw $e;
		}

		return $messenger;
	}

	/**
	 * @param int $projectId
	 * @param int $messengerId
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public static function deleteMessenger(int $projectId, int $messengerId) {
		self::$method = 'delete';
		self::$subUrl = $projectId . '/messenger/' . $messengerId;

		try {
			$response = self::getResponse();
		} catch (\Exception $e) {
			throw $e;
		}

		return true;
	}

}
