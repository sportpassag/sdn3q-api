<?php

namespace SDN3Q\Model;

use DateTime;
use MintWare\DMM\DataField;

class ChannelLog {

	/**
	 * @var int
	 * @DataField(name="Id", type="int")
	 */
	public $id;

	/**
	 * @var string
	 * @DataField(name="Description", type="string")
	 */
	public $text;

	/**
	 * @var string
	 * @DataField(name="LogLevel", type="string")
	 */
	public $logLevel;

	/**
	 * @var DateTime
	 * @DataField(name="CreatedAt", type="datetime")
	 */
	public $createdAt;
}
