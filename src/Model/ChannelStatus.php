<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class ChannelStatus extends BaseModel {

	/**
	 * @var int
	 * @DataField(name="id", type="int")
	 */
	public $id;

	/**
	 * Online Status of the Channel
	 * @var bool
	 * @DataField(name="IsOnline", type="boolean")
	 */
	public $isOnline;

	/**
	 * @var bool
	 * @DataField(name="isPrimary", type="boolean")
	 * @DataField(name="IsPrimary", type="boolean")
	 */
	public $isPrimary;

	/**
	 * @var string
	 * @DataField(name="Origin", type="string")
	 */
	public $origin;

	/**
	 * @var string
	 * @DataField(name="VideoFormat", type="string")
	 */
	public $videoFormat;

	/**
	 * @var string
	 * @DataField(name="VideoFormatString", type="string")
	 */
	public $videoFormatString;

	/**
	 * @var int
	 * @DataField(name="VideoBitRate", type="int")
	 * @DataField(name="VideoBitrate", type="int")
	 */
	public $videoBitRate;

	/**
	 * @var float
	 * @DataField(name="VideoFPS", type="float")
	 */
	public $videoFps;

	/**
	 * @var int
	 * @DataField(name="VideoWidth", type="int")
	 */
	public $videoWidth;

	/**
	 * @var int
	 * @DataField(name="VideoHeight", type="int")
	 */
	public $videoHeight;

	/**
	 * @var string
	 * @DataField(name="AudioFormat", type="string")
	 */
	public $audioFormat;

	/**
	 * @var string
	 * @DataField(name="AudioFormatString", type="string")
	 */
	public $audioFormatString;

	/**
	 * @var int
	 * @DataField(name="AudioBitRate", type="int")
	 * @DataField(name="AudioBitrate", type="int")
	 */
	public $audioBitRate;
}
