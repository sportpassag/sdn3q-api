<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class MessengerRequirement {

	/**
	 * @var int
	 * @DataField(name="Id", type="int")
	 */
	public $id;

	/**
	 * @var string
	 * @DataField(name="EventName", type="string")
	 */
	public $eventName;
}
