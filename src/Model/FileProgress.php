<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class FileProgress extends BaseModel
{
	/**
	 * File status
	 *
	 * @var string
	 * @DataField(name="Status", type="string")
	 */
	public $status;

	/**
	 * The source (uploaded) file is stored in cdn
	 *
	 * @var bool
	 * @DataField(name="SourceIsStored", type="boolean")
	 */
	public $sourceIsStored;

	/**
	 * The source file has been analyzed, encoding is started or the file is pushed into the pipeline
	 *
	 * @var bool
	 * @DataField(name="SourceIsAnalyzed", type="boolean")
	 */
	public $sourceIsAnalyzed;


	/**
	 * The progress of downloading the source file to the encoder engine
	 *
	 * @var int
	 * @DataField(name="SourceEncoderDownloadProgress", type="int")
	 */
	public $sourceEncoderDownloadProgress;

	/**
	 * Properties
	 *
	 * @var int
	 * @DataField(name="EncodingJobsProgressAVG", type="int")
	 */
	public $encodingJobsProgressAVG;
}
