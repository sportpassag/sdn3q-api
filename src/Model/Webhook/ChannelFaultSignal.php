<?php

namespace SDN3Q\Model\Webhook;

use MintWare\DMM\DataField;
use SDN3Q\Model\Channel;

class ChannelFaultSignal {

	/**
	 * @var int
	 * @DataField(name="ProjectId", type="int")
	 */
	public $projectId;

	/**
	 * @var Channel
	 * @DataField(name="Channel", type="\SDN3Q\Model\Channel")
	 */
	public $channel;
}
