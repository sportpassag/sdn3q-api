<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class ChannelIngest extends BaseModel {

	/**
	 * Type of Channel input
	 *
	 * @var string
	 * @DataField(name="StreamInType", type="string")
	 */
	public $streamInType;

	/**
	 * Connection Details for Input
	 *
	 * @var StreamInConnection
	 * @DataField(name="StreamInConnection", type="\SDN3Q\Model\StreamInConnection")
	 */
	public $streamInConnection;

	/**
	 * @var bool
	 * @DataField(name="AutomaticTranscoding", type="bool")
	 */
	public $automaticTranscoding;

	/**
	 * @var int
	 * @DataField(name="TimeshiftDuration", type="int")
	 */
	public $timeshiftDuration;

	/**
	 * @var bool
	 * @DataField(name="IsSynced", type="bool")
	 */
	public $isSynced;

	/**
	 * @var bool
	 * @DataField(name="SourceStabilizing", type="bool")
	 * @DataField(name="_Source_Stabilizing", type="bool")
	 * @deprecated split into ingestStabilizing & ingestFailover
	 */
	public $sourceStabilizing;

	/**
	 * Bitte diese Option nur aktivieren, wenn Sie primary und secondary Signale versenden.
	 * Wenn sowohl primary also auch secondary ausfallen, wird der Ausfall mit einem Schwarzen Bild überbrückt.
	 *
	 * @var bool
	 * @DataField(name="IngestFailover", type="bool")
	 */
	public $ingestFailover;

	/**
	 *  Wenn diese Option aktive ist und automatischer Failover nicht aktiv ist, werden temporäre Ausfälle des Eingangssignals mit dem Störbild
	 *  überbrückt. Das Bild kann in den Metadaten verwaltet werden. Wenn kein Bild hochgeladen ist, wird ein schwarzes Bild verwendet.
	 *
	 * @var bool
	 * @DataField(name="IngestStabilizing", type="bool")
	 */
	public $ingestStabilizing;

	/**
	 * @var IngestPoint[]
	 * @DataField(name="IngestPoints", type="\SDN3Q\Model\IngestPoint[]")
	 */
	public $ingestPoints;

}
