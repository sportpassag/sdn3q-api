<?php

namespace SDN3Q\Model;

use DateTime;
use MintWare\DMM\DataField;

class Channel extends BaseModel {

    /**
     * @var int
     * @DataField(name="Id", type="int")
     */
    public $id;

    /**
     * Status of the Channel
     * @var \SDN3Q\Model\ChannelStatus[]
     * @DataField(name="ChannelStatus",type="\SDN3Q\Model\ChannelStatus[]")
     */
    public $channelStatus;

    /**
     * Channel Metadata
     * @var \SDN3Q\Model\ChannelMetaData
     * @DataField(name="ChannelMetadata",type="\SDN3Q\Model\ChannelMetaData")
     */
    public $channelMetadata;

    /**
     * Project
     *
     * @var \SDN3Q\Model\Project
     * @DataField(name="Project",type="\SDN3Q\Model\Project")
     */
    public $project;

    /**
     * Time of creation
     *
     * @var DateTime
     * @DataField(name="CreatedAt", type="datetime")
     */
    public $createdAt;

    /**
     * Time of last update
     * @var DateTime
     * @DataField(name="LastUpdatedAt", type="datetime")
     */
    public $lastUpdatedAt;

    /**
     * @var bool
     * @DataField(name="IsStopping", type="boolean")
     */
    public $isStopping;
}
