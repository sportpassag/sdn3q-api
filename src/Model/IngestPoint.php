<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class IngestPoint {

	/**
	 * @var int
	 * @DataField(name="Id", type="int")
	 */
	public $id;

	/**
	 * @var string
	 * @DataField(name="Name", type="string")
	 */
	public $name;

	/**
	 * @var bool
	 * @DataField(name="Primary", type="boolean")
	 */
	public $isPrimary;

	/**
	 * @var string
	 * @DataField(name="ServerURI", type="string")
	 */
	public $serverUri;

	/**
	 * @var string
	 * @DataField(name="SecureServerURI", type="string")
	 */
	public $secureServerUri;

	/**
	 * @var string
	 * @DataField(name="StreamName", type="string")
	 */
	public $streamName;
}
