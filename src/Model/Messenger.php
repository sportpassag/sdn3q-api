<?php

namespace SDN3Q\Model;

use MintWare\DMM\DataField;

class Messenger {

	/**
	 * @var int
	 * @DataField(name="Id", type="int")
	 */
	public $id;

	/**
	 * @var int
	 * @DataField(name="ProjectId", type="int")
	 */
	public $projectId;

	/**
	 * @var string
	 * @DataField(name="Name", type="string")
	 */
	public $name;

	/**
	 * @var string
	 * @DataField(name="Description", type="string")
	 */
	public $description;

	/**
	 * @var string
	 * @DataField(name="WebhookURI", type="string")
	 */
	public $webhookUri;
	/**
	 * @var string
	 * @DataField(name="WebhookMethod", type="string")
	 */
	public $webhookMethod;

	/**
	 * @var MessengerRequirement
	 * @DataField(name="MessengerRequirement", type="\SDN3Q\Model\MessengerRequirement")
	 */
	public $messengerRequirement;
}
